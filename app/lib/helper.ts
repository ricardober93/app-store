export const COPCurrency = new Intl.NumberFormat('CO-es', {
    style: 'currency',
    currency: 'COP',
});