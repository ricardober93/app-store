import prisma from "@lib/prisma";

import { NextRequest, NextResponse } from "next/server";

export const dynamicParams = true;
export async function GET(request: NextRequest, { params }: any) {
  const id = params.id;

  const Product = await prisma.product.findUnique({
    where: {
      id: Number(id),
    },
    select: {
      id: true,
      title: true,
      imageUrls: true,
      price: true,
      colors: true,
      freeShip: true,
      rate: true,
      description: true,
    },
  });

  if (Product) {
    return NextResponse.json({ Product });
  } else {
    return NextResponse.json({ error: "Products not found" });
  }
}
