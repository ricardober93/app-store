import prisma from "@lib/prisma";

import { NextRequest, NextResponse } from "next/server";

export async function GET(req: NextRequest) {

  const ProductList = await prisma.product.findMany({
   select: {
     id: true,
     title: true,
     imageUrls: true,
     price: true,
   }
  });

  if (ProductList) {
    return NextResponse.json(ProductList);
    
  }else{
    return NextResponse.json({ error: "Products not found" });
  }
}


export async function POST(req: NextRequest) {
    const { title, description, imageUrls, price, colors, freeShip, rate  } = await req.json();

  const product = await prisma.product.create({
    data: {
      title,
      description,
      imageUrls,
      price,
      colors,
      freeShip,
      rate
    },
  });
  if (!product) {
    return NextResponse.json({ error: "product already exists"  });
  } else {
    return NextResponse.json({ message: 'product created',  status: 204 });
  }
}
