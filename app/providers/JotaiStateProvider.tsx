"use client";

import { myStore } from "@apps/store";
import { Provider } from "jotai";

const JoitaProvider = ({ children }: { children: React.ReactNode }) => {
  return <Provider store={myStore}>{children}</Provider>;
};

export default JoitaProvider;
