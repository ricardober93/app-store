"use client";
import { css } from "@styles/css";
import { signIn } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { SlHome, SlLock } from "react-icons/sl";
import LoadingDots from "../../components/loading-dots";

type Inputs = {
  email: string;
  password: string;
};

export default function Page() {
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs> = (form) => {
    setIsLoading(true);

    signIn("credentials", {
      redirect: false,
      email: form.email,
      password: form.password,
      // @ts-ignore
    }).then(async ({ error }) => {
      if (error) {
        setIsLoading(false);
        toast.error(error);
      } else {
        router.push("/");
        toast.success('Inicio de sesión exitoso');
      }
    }).catch(() => {
      setIsLoading(false);
      toast.error("Error al iniciar sesión");
    });
  };
  return (
    <main
      className={css({
        minH: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        bg: "white",
        position: "relative",
      })}>
      <section
        className={css({
          bg: "white",
          p: "4",
          rounded: "24px",
          width: {
            base: "100%",
            md: "500px",
          },
          position: {
            base: "absolute",
            md: "relative",
          },
          bottom: {
            base: "0",
            md: "auto",
          },
          marginX: "auto",
          shadow: "lg",
          border: "1px solid",
          borderColor: "gray.200",
        })}>
        <form
          className={css({
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            gap: 2,
          })}
          onSubmit={handleSubmit(onSubmit)}>
          <h3 className={css({ fontSize: "2xl", fontWeight: "bold", my: "2rem" })}>Iniciar sesión</h3>
          <div
            className={css({
              width: "100%",
              display: "grid",
              gap: 2,
            })}>
            <label
              className={css({
                width: "100%",
              })}>
              Email
            </label>
            <div
              className={css({
                width: "100%",
                position: "relative",
              })}>
              <div className={css({ position: "absolute", top: "1.2rem", left: "1rem", color: "orange.500" })}>
                <SlHome
                  size={20}
                  strokeWidth={2}
                />
              </div>
              <input
                className={css({
                  width: "100%",
                  border: "2px solid",
                  borderColor: "orange.500",
                  p: "1rem",
                  color: "gray.900",
                  pl: "3rem !important",
                  rounded: "24px",
                  bg: "transparent",
                  _focus: {
                    borderColor: "orange.700",
                  },
                  _selected: {
                    borderColor: "none",
                  },
                })}
                type="email"
                placeholder="user@example.com"
                {...register("email")}
              />
            </div>
          </div>

          <div
            className={css({
              width: "100%",
              display: "grid",
              gap: 2,
            })}>
            <label
              className={css({
                width: "100%",
              })}>
              Contraseña
            </label>
            <div
              className={css({
                width: "100%",
                position: "relative",
              })}>
              <div className={css({ position: "absolute", top: "1.2rem", left: "1rem", color: "orange.500" })}>
                <SlLock
                  size={20}
                  strokeWidth={2}
                />
              </div>
              <input
                className={css({
                  width: "100%",
                  border: "2px solid",
                  borderColor: "orange.500",
                  p: "1rem",
                  color: "gray.900",
                  pl: "3rem !important",
                  rounded: "24px",
                  bg: "transparent",
                  _focus: {
                    borderColor: "orange.700",
                  },
                  _selected: {
                    borderColor: "none",
                  },
                })}
                type="password"
                placeholder="*******"
                {...register("password", { required: true, minLength: 6 })}
              />
            </div>
            {errors?.password?.type === "required" && <span className={css({ color: "red" })}>This field is required</span>}
            {errors?.password?.type === "minLength" && <span className={css({ color: "red" })}>This field needs to be at least 6 characters</span>}
          </div>

          <button
            disabled={isLoading}
            type="submit"
            className={css({
              mt: "1rem",
              width: "100%",
              p: "1rem",
              rounded: "24px",
              bg: "orange.500",
              color: "white",
              fontWeight: "bold",
              fontSize: "lg",
              cursor: "pointer",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            })}>
            {isLoading ? <LoadingDots color="#fff" /> : "Entrar"}
          </button>
        </form>
      </section>
    </main>
  );
}
