import { atom, createStore } from "jotai";

export const myStore = createStore();

export interface Products {
  id: string;
  name: string;
  image: string;
  price: number;
  description: string;
  rating: number;
  like: boolean;
}

export const ProductList = atom<Products[]>([]);
myStore.set(ProductList, [
  {
    id: "1",
    name: "product 1",
    image: "https://images.unsplash.com/photo-1517694712202-14dd9538aa97?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8Y29tcHV0ZXJ8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=500&q=60",
    price: 100,
    description: "description del mejor producto del mundo",
    rating: 4,
    like: true,
  },
  {
      id: "2",
      name: "product 2",
      image: "https://images.unsplash.com/photo-1517694712202-14dd9538aa97?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8Y29tcHV0ZXJ8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=500&q=60",
      price: 200,
      description: "description del mejor producto del mundo",
      rating: 4,
      like: false,

  },
  {
      id: "3",
      name: "product 3",
      image: "https://images.unsplash.com/photo-1517694712202-14dd9538aa97?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8Y29tcHV0ZXJ8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=500&q=60",
      price: 300,
      description: "description del mejor producto del mundo",
      rating: 4,
      like: false,
  }
]);
const unsub = myStore.sub(ProductList, () => {
  console.log("countAtom value is changed to", myStore.get(ProductList));
});
