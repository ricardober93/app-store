import { css } from "@styles/css";

const LoadingDots = ({ color = "#000" }: { color?: string }) => {
  return (
    <span
      className={css({
        "display": " flex",
        "alignItems": " center",
        "& span": {
          animationName: "loading",
          animationDuration: "1.4s",
          animationIterationCount: "infinite",
          animationFillMode: "both",
          width: "5px",
          h: "5px",
          borderRadius: "50%",
          display: "inline-block",
          margin: "0 1px",
        },

        "& span:nth-of-child(2)": {
          animationDelay: "0.2s",
        },
      })}>
      <span style={{ backgroundColor: color }} />
      <span style={{ backgroundColor: color }} />
      <span style={{ backgroundColor: color }} />
    </span>
  );
};

export default LoadingDots;
