import { css } from "@styles/css";
import Image from "next/image";

export type GalleryDetailProps = {};
export const GalleryDetail: React.FC<GalleryDetailProps> = ({}) => {
  return (
    <div
      className={css({
        w: "100%",
        h: "65%",
      })}>
      <Image
        className={css({
          w: "100%",
          h: "80%",
          py: "4",
          px: "2",
          rounded: "3xl",
        })}
        width={500}
        height={500}
        alt="imagen producto"
        src={"https://images.unsplash.com/photo-1539683255143-73a6b838b106?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=685&q=80"}
      />

      <section
        className={css({
          w: "100%",
          display: "flex",
          gap: "3",
          alignItems: "center",
          overflowY: "scroll",
          scrollBehavior: "smooth",
        })}>
        {[0, 0, 0, 0, 0, 1].map((item, i) => (
          <Image
            key={i}
            className={css({
              w: "90px",
              minW: "90px",
              h: "90px",
              py: "4",
              px: "2",
              objectFit: "cover",
              rounded: "3xl",
            })}
            width={500}
            height={500}
            alt="imagen producto"
            src={"https://images.unsplash.com/photo-1539683255143-73a6b838b106?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=685&q=80"}
          />
        ))}
      </section>
    </div>
  );
};

export default GalleryDetail;
