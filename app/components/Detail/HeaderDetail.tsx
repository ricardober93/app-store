"use client";
import { css } from "@styles/css";
import { useRouter } from "next/navigation";
import { SlArrowLeft, SlHeart, SlShare } from "react-icons/sl";

export type HeaderDetailProps = {};
export const HeaderDetail: React.FC<HeaderDetailProps> = ({}) => {
  const router = useRouter();
  return (
    <nav
      className={css({
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        w: "100%",
        px: "2",
      })}>
      <div
        onClick={() => {
          router.back();
        }}
        className={css({
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          p: "4",
          bg: "white",
          color: "gray.400",
          rounded: "full",
        })}>
        <SlArrowLeft size={12} />
      </div>


      <div
        className={css({
            flex: '1',
        })}>
        </div>

      <div
        className={css({
          display: "flex",
          alignItems: "center",
          gap: "4",
        })}>
        <div
         onClick={() => {
           
         }}
          className={css({
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            p: "4",
            bg: "white",
            color: "gray.400",
            rounded: "full",
          })}>
          <SlHeart size={12} />
        </div>
        <div
          onClick={() => {
           
          }}
          className={css({
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            p: "4",
            bg: "white",
            color: "gray.400",
            rounded: "full",
          })}>
          <SlShare size={12} />
        </div>
      </div>
    </nav>
  );
};

export default HeaderDetail;
