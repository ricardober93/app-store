"use client";
import { Product } from "@prisma/client";
import { css } from "@styles/css";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import { SlHeart } from "react-icons/sl";
import { grid } from "./../../../styled-system/patterns";
import Title from "./Title";

export type ListProductProps = {};
export const ListProduct: React.FC<ListProductProps> = ({}) => { 
 const [products, setProducts] = useState<Product[]>([]);

 useEffect(() => {
   fetch('api/products').then(res => res.json()).then(data => {
     setProducts(data)
   }).catch(err => {
     console.log(err)
   })
 }, [])

  return (
    <section
      className={css({
        display: "flex",
        flexDir: "column",
        gap: "2",
        mt: "3",
        mb: "5.5rem",
      })}>
      <Title
        fontSize="lg"
        fontWeight="bold"
        text="Novedades"
      />
      <div
        className={grid({
          columns: {
            base: 2,
            sm: 4,
          },
          gap: "3",
        })}>
        {products && products.map((item, i) => (
          <Link
            href={`detail/${item.id}`}
            key={item.id}
            className={css({
              display: "flex",
              flexDir: "column",
              bg: "gray.200",
              justifyContent: "center",
              alignItems: "center",
              rounded: "lg",
              p: "2",
              gap: "2",
              cursor: "pointer",
            })}>
            <div
              className={css({
                w: "100%",
                display: "flex",
                justifyContent: "flex-end",
              })}>
              <div
                className={css({
                  w: "2rem",
                  h: "2rem",
                  p: "1",
                  rounded: "full",
                  bgColor: `${item.freeShip ? "orange.500" : "white"}`,
                  color: `${item.freeShip ? "white" : "orange.500"}`,
                  display: "grid",
                  placeContent: "center",
                  placeItems: "center",
                })}>
                  <SlHeart width={16} />
              </div>
            </div>
            <Image
              className={css({
                minW: 120,
                rounded: "lg",
              })}
              width={120}
              height={120}
              alt="Producto 1"
              src={item.imageUrls[0]}
            />
            <Title
              fontSize="xs"
              fontWeight="medium"
              text={item.title}
            />
            <Title
              fontSize="sm"
              fontWeight="bold"
              text={`$ ${item.price}`}
            />
          </Link>
        ))}
      </div>
    </section>
  );
};

export default ListProduct;
