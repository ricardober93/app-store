import { css } from "@styles/css";

export type CategoriesProps = {};
export const Categories: React.FC<CategoriesProps> = ({}) => {
  return (
    <section
      id="Categories"
      className={css({
        w: "100%",
        h: "auto",
        display: "flex",
        gap: "1rem",
        overflowX: "scroll",
        scrollBehavior: "smooth",
        py: "1",
      })}>
      {[0, 0, 0, 0, 0, 0].map((d, i) => (
        <div
          key={i}
          className={css({
            w: "100%",
            px: "6",
            py: "2",
            bg: "white",
            rounded: "3xl",
            border: "1px",
            borderStyle: "solid",
            borderColor: "gray.200",
            cursor: "pointer",
          })}>
          {`categoria${i}`}
        </div>
      ))}
    </section>
  );
};

export default Categories;
