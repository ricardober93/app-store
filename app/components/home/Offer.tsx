'use client';
import { css } from "@styles/css";
import Title from "./Title";
import Image from "next/image";
import Link from "next/link";
import { COPCurrency } from "@apps/lib/helper";

export type OffersProps = {   }
export const Offers: React.FC<OffersProps> = ({  }) => {
    return (
        <section
        id="SuperOffer"
        className={css({
          w: "100%",
          mt: "3",
        })}>
        <div
          className={css({
            w: "100%",
            h: "auto",
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          })}>
          <Title
            fontSize="xl"
            fontWeight="bold"
            text="Super Ofertas"
          />

        </div>

        <main
          className={css({
            w: "100%",
            h: "auto",
            display: "flex",
            gap: "1rem",
            overflowX: "scroll",
            scrollBehavior: "smooth",
            py: "1",
          })}>
          {[0, 0, 0, 0, 0, 0].map((d, i) => (
            <Link
            href={`detail/${i}`}
              key={i}
              className={css({
                width: "400px",
                p: "3",
                bg: "gray.200",
                rounded: "lg",
                display: "flex",
                flexDir: "column",
                gap: "2",
                shadow: "sm",
              })}>
              <div
                className={css({
                  mr: "auto",
                  bg: "white",
                  px: "2",
                  color: "orange.500",
                  fontSize: "2xs",
                  rounded: "sm",
                })}>
                Envio gratis
              </div>

              <Image
                className={css({
                  minW: 120,
                  rounded: "lg",
                })}
                width={120}
                height={120}
                alt="Producto 1"
                src="https://images.unsplash.com/photo-1517694712202-14dd9538aa97?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8Y29tcHV0ZXJ8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=500&q=60"
              />
              <Title
              fontSize="xs"
              fontWeight="medium"
              text="Sony WH/1000XM4"
              />
                <Title
              fontSize="sm"
              fontWeight="bold"
              text={COPCurrency.format(34000)}
              />
            </Link>
          ))}
        </main>
      </section>
    )
};

export default Offers;