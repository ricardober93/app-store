import { css } from "@styles/css";

export type TitleProps = {
  fontSize: string;
  fontWeight: string;
  text: string;
};
export const Title: React.FC<TitleProps> = ({ fontSize, fontWeight, text }) => {
  return (
    <h1
      className={css({
        fontWeight,
        fontSize,
      })}>
      {text}
    </h1>
  );
};

export default Title;
