import { css } from "@styles/css";
import { SlBell, SlMagnifier } from "react-icons/sl";


export type HeaderHomeProps = {  }
export const HeaderHome: React.FC<HeaderHomeProps> = ({  }) => {
    
    return (
        <section
        id="header"
        className={css({
          w: "100%",
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          gap: "2rem",
        })}>
        <div
          className={css({
            w: "90%",
            pos: "relative",
          })}>
          <div
            className={css({
              pos: "absolute",
              left: "1rem",
              top: "1rem",
              color: "gray.400",
            })}>
            <SlMagnifier
              size={20}
              strokeWidth={3}
            />
          </div>
          <input
            className={css({
              w: "100%",
              rounded: "full",
              pl: "3rem",
              py: "1rem",
            })}
            type="text"
          />
        </div>

        <div
          className={css({
            w: "10%",
            height: "100%",
            pos: "relative",
            display:'flex',
            justifyContent:'flex-end'
          })}>
          <div
            className={css({
              w: "15px",
              h: "15px",
              bg: "red",
              rounded: "full",
              pos: "absolute",
              top: "-0.5",
              right: "-0.5",
              zIndex: "10",
            })}></div>
          <section
            className={css({
              maxW: "60px",
              minW: "60px",
              w: "100%",
              h: "100%",
              bg: "white",
              rounded: "full",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              py: "1rem",
              color: "gray.400",
            })}>
            <SlBell
              size={20}
              strokeWidth={3}
            />
          </section>
        </div>
      </section>
    )
};

export default HeaderHome;