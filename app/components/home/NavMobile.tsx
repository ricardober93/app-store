"use client";
import { css } from "@styles/css";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { SlBasket, SlHeart, SlHome, SlUser } from "react-icons/sl";

export type NavMobileProps = {};
export const NavMobile: React.FC<NavMobileProps> = ({}) => {
  const pathname = usePathname();
  return (
    <section
      id="nav"
      className={css({
        position: "fixed",
        w: "100%",
        h: "auto",
        display: "flex",
        bottom: "0",
        left: "0",
        right: "0",
        py: "6",
        px: "3",
        bg: "white",
        zIndex: "10",
        rounded: "2xl",
        placeContent: "center",
      })}>
      <nav
        className={css({
          w: "100%",
          px: "6",
          "display": "flex",
          "justifyContent": "space-between",
          "alignItems": "center",
          "gap": "1rem",
        })}>
        <Link
          href="/"
          className={css({
            color: `${pathname === "/" ? "orange.500" : "gray.500"}`,
            display: "flex",
            alignItems: "center",
            gap: "1",
            cursor: "pointer",
            p:'2'
          })}>
          <SlHome />
          {pathname === "/" ? <span>Home</span> : null}
        </Link>
        <Link
          href="/cart"
          className={css({
            color: `${pathname === "/cart" ? "orange.500" : "gray.500"}`,
            display: "flex",
            alignItems: "center",
            gap: "1",
            cursor: "pointer",
            p:'2'
          })}>
          <SlBasket />
          {pathname === "/cart" ? <span>cart</span> : null}
        </Link>
        <Link
          href="/guardados"
          className={css({
            color: `${pathname === "/guardados" ? "orange.500" : "gray.500"}`,
            display: "flex",
            alignItems: "center",
            gap: "1",
            cursor: "pointer",
            p:'2'
          })}>
          <SlHeart />
          {pathname === "/guardados" ? <span>guardados</span> : null}
        </Link>
        <Link
          href="/perfil"
          className={css({
            color: `${pathname === "/perfil" ? "orange.500" : "gray.500"}`,
            display: "flex",
            alignItems: "center",
            gap: "1",
            cursor: "pointer",
            p:'2'
          })}>
          <SlUser />
          {pathname === "/perfil" ? <span>perfil</span> : null}
        </Link>
      </nav>
    </section>
  );
};

export default NavMobile;
