"use client";
import GalleryDetail from "@apps/components/Detail/GalleryDetail";
import HeaderDetail from "@apps/components/Detail/HeaderDetail";
import Title from "@apps/components/home/Title";
import { COPCurrency } from "@apps/lib/helper";
import { Product } from "@prisma/client";
import { css } from "@styles/css";
import { useParams, useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { SlStar } from "react-icons/sl";

export default function Page() {
  const params = useParams();
  const [product, setProduct] = useState<Product>();

  useEffect(() => {
    fetch(`../api/products/${params.id}`)
      .then((res) => res.json())
      .then(({ Product }) => {
        setProduct(Product);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [params.id]);

  return (
    <main
      className={css({
        bg: "gray.100",
        minH: "100%",
        h: "100%",
        pt: "5",
      })}>
      {product ? (
        <>
          <HeaderDetail />
          <GalleryDetail />
          <div
            className={css({
              bg: "white",
              h: "30%",
              py: "6",
              px: "4",
              shadow: "lg",
              roundedTop: "3xl",
              display: "flex",
              flexDirection: "column",
              gap: "3",
            })}>
            <section
              className={css({
                display: "flex",
                w: "100%",
                justifyContent: "space-between",
                alignItems: "center",
              })}>
              <p
                className={css({
                  fontSize: "sm",
                  fontWeight: "normal",
                  color: "gray.400",
                })}>
                {product?.freeShip ? "Envío gratis" : null}
              </p>

              <div
                className={css({
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  gap: "2",
                })}>
                <SlStar size={16} />
                <Title
                  fontSize="sm"
                  fontWeight="normal"
                  text={`${product?.rate}`}
                />
              </div>
            </section>

            <Title
              fontSize="lg"
              fontWeight="bold"
              text={product?.title}
            />
            <p
              className={css({
                fontSize: "sm",
                fontWeight: "normal",
                color: "gray.400",
              })}>
              {product?.description}
            </p>

            <p
              className={css({
                fontSize: "lg",
                fontWeight: "medium",
                color: "orange.500",
              })}>
              {COPCurrency.format(Number(product?.price))}
            </p>

            <div
              className={css({
                w: "100%",
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
              })}>
              <fieldset
                className={css({
                  display: "flex",
                  gap: "2",
                  alignItems: "center",
                })}>
                <button
                  className={css({
                    p: "2",
                    fontSize: "sm",
                  })}>
                  -
                </button>
                <input
                  className={css({
                    w: "20px",
                  })}
                  defaultValue={1}
                  type="text"
                />
                <button
                  className={css({
                    p: "2",
                    fontSize: "sm",
                  })}>
                  +
                </button>
              </fieldset>

              <button
                className={css({
                  w: "60%",
                  fontSize: "sm",
                  bg: "orange.500",
                  py: "2",
                  px: "4",
                  fontWeight: "normal",
                  color: "white",
                  rounded: "2xl",
                })}>
                Agregar al carrito
              </button>
            </div>
          </div>
        </>
      ) : null}
    </main>
  );
}
