
import Categories from "@components/home/Categories";
import HeaderHome from "@components/home/Header-home";
import ListProduct from "@components/home/ListProducts";
import Offers from "@components/home/Offer";
import { css } from "@styles/css";
import Image from "next/image";
import ImageBanner from "@public/principal_home.png";
import getCurrentUser from "../actions/getCurrentUser";

export default async function Page() {
  const user = await getCurrentUser();
  return (
    <main className={css({ h: "100%", bg: "gray.100 ", pt: "2rem", px: "1rem",})}>
      <HeaderHome />

      <section>
        <Image
          className={css({
            w: "100%",
            h: "auto",
            bgSize: "cover",
            objectFit: "contain",
          })}
          width={500}
          height={320}
          src={ImageBanner.src}
          alt="Principal"
        />
      </section>
      <Categories />
      <Offers />
      <ListProduct />
    </main>
  );
}
