import ClientOnly from "@apps/components/client";
import NavMobile from "@apps/components/home/NavMobile";

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <main>
      <ClientOnly>
        <NavMobile />
      </ClientOnly>
      {children}
    </main>
  );
}
