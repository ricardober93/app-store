/*
  Warnings:

  - You are about to drop the column `favorite` on the `Product` table. All the data in the column will be lost.
  - Changed the type of `freeShip` on the `Product` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `rate` on the `Product` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- AlterTable
ALTER TABLE "Product" DROP COLUMN "favorite",
DROP COLUMN "freeShip",
ADD COLUMN     "freeShip" BOOLEAN NOT NULL,
DROP COLUMN "rate",
ADD COLUMN     "rate" DOUBLE PRECISION NOT NULL;
